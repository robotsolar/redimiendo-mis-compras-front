import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { StoreTicket } from '../models/ticket';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private http: HttpClient,) { }

/**
 * @description regresa todos los tickets del usuario
 * @returns 
 */
  getUserTickets(){
    return this.http.get(`${environment.url}tickets`);
  }
 
/**
 * 
 * @param ticket 
 * @returns 
 */
  storeTicket(ticket : StoreTicket): Observable<any> {
    const headers = new HttpHeaders({'no-change-content':''});
    let formModel = new FormData();
    formModel.append('store_name', ticket.storename);
    formModel.append('total', ticket.total.toString());
    formModel.append('image', ticket.image);
    return  this.http.post(`${environment.url}tickets`, formModel, {headers: headers})
  }
}
