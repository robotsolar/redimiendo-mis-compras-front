import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { UserRegistry } from '../models/User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,) { }

  /**
   * @description Registra un usuario
   * @param userData - src/models/user.ts
   * @returns Observable
   */
  registerUser(userData : UserRegistry) : Observable<any>{
    return this.http.post(`${environment.url}auth/register`, userData);
  }

  /**
   * @description Loguea a usuario con nombre de usuario y contraseña
   * @param username - string
   * @param password - string
   * @returns Observable 
   */
  signInWithUsernameAndPassword(username:string, password:string) : Observable<any>{
    const requestData = {
      username : username,
      password : password
    }
    return this.http.post(`${environment.url}auth/login`, requestData);
  }

  checkUsername(username:string){
    return this.http.get(`${environment.url}auth/exists/${username}`);
  }

  /**
   *  remueve el token del usuario en el servidor y en el app
   * @returns 
   */
  logout() : Observable<any>{
    return this.http.post(`${environment.url}auth/logout`,{});
  }
}
