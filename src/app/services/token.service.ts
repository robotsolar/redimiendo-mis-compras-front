import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }
 
setToken(token:string) {
  return Storage.set({
    key: 'token-app',
    value: token
  });
}

getToken() {
  return Storage.get({ key: 'token-app' });
}

deleteToken() {
  return Storage.remove({ key: 'token-app' });
}

}
