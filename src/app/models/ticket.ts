export interface StoreTicket {
    storename: string
    total: number
    image: File
}

export interface SuccessResponseTickets {
    success: boolean
    payload: StoreTicket[]

}