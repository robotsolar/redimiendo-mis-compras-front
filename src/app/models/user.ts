export interface User {
    name: string
    username: string
    birthday: string
    phone: string
}

export interface UserRegistry {
    birthday: string
    c_password: string
    name: string
    password: string
    phone: string
    privacy: boolean
    terms: boolean
    username: string
}

export interface SuccessRegistry {
    success: boolean
    access_token: string
    expiration: string
    payload: User
}