import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TermsAndConditionsPage } from "../terms-and-conditions/terms-and-conditions.page";
import { CreateTicketPage } from "../create-ticket/create-ticket.page";
import { AuthService } from "src/app/services/auth.service";
import { TicketService } from "src/app/services/ticket.service";
import { TokenService } from "src/app/services/token.service";
import { Router } from '@angular/router';
import { StoreTicket, SuccessResponseTickets } from 'src/app/models/ticket';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  nameApp = "Redimiendo mis compras"
  name;
  loading: boolean = true;
  tickets: StoreTicket[] = []
  selectedTicket: StoreTicket;
  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private tokenService: TokenService,
    private ticketservice: TicketService,
    public router: Router
  ) {
    this.name = localStorage.getItem('name');
  }

  ngOnInit() {
    this.getTicketFromServer();
  }

  getTicketFromServer() {
    this.ticketservice.getUserTickets().subscribe((res: SuccessResponseTickets) => {
      this.tickets = res.payload;
      this.loading = false;
    });
  }

  async logout() {
    const response = await this.authService.logout().toPromise();
    if (response.success) {
      await this.tokenService.deleteToken();
      this.router.navigateByUrl('login');
    }
  }

  async viewTermsAndConditions() {
    const modal = await this.modalController.create({
      component: TermsAndConditionsPage,
    });

    await modal.present();
  }

  async createTicket() {
    const modal = await this.modalController.create({
      component: CreateTicketPage,
    });

    await modal.present();
    await modal.onDidDismiss()
    this.getTicketFromServer();
  }


}
