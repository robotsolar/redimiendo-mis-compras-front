import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController, ModalController } from '@ionic/angular';
import { StoreTicket } from 'src/app/models/ticket';
import { TicketService } from "src/app/services/ticket.service";

@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.page.html',
  styleUrls: ['./create-ticket.page.scss'],
})
export class CreateTicketPage {
  loading: boolean = false;
  imageFormatValid: boolean;
  imageSizeValid: boolean;
  file: File;
  constructor(private modalController: ModalController, private ticketService: TicketService, private alertController: AlertController) { }
  dismiss(data?) {
    this.modalController.dismiss(data);
  }

  onSelect(event) {
    const types = ['image/jpeg', 'image/png', 'image/gif', 'image/jpg',];
    console.log(event);
    this.file = event.addedFiles[0];
    this.imageFormatValid = types.includes(this.file.type);
    this.imageSizeValid = (this.file.size < 2000000);

  }
  onRemove(event) {
    // console.log(event);
    this.file = null;

  }

  async storeTicket(form: NgForm) {
    console.log(form);
    if (form.valid && this.imageFormatValid && this.imageSizeValid) {
      this.loading = true;
      const storeTicket: StoreTicket = {
        image: this.file,
        storename: form.value.store_name,
        total: form.value.total
      };
      const response = await this.ticketService.storeTicket(storeTicket).toPromise();
      this.loading = false;
      if (response.success) {
        const alert = await this.alertController.create({
          header: 'Bien hecho',
          message: 'Tu ticket ha sido guardado 🎉',
          buttons: [{
              text: 'Cerrar',
              handler: () => {
                this.dismiss({ created: true })
              }
            },{
              text: 'Capturar otro ticket',
              handler: () => {

                form.resetForm();
                this.file = null;
              }
            }]
        });
        await alert.present();
      }
    }
  }
}
