import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { SuccessRegistry } from 'src/app/models/User';
import { AuthService } from "src/app/services/auth.service";
import { TokenService } from "src/app/services/token.service";

import { NoticeOfPrivacyPage } from "../../notice-of-privacy/notice-of-privacy.page";
import { TermsAndConditionsPage } from "../../terms-and-conditions/terms-and-conditions.page";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  NoticeofPrivacy = NoticeOfPrivacyPage;
  termsAndConditions = TermsAndConditionsPage;
  usernameExists:boolean;
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    public router: Router,
    private modalController: ModalController
  ) { }

  private usernameSource = new Subject<string>();
  
  ngOnInit(){
    this.usernameSource.pipe(
      debounceTime(1000)
    ).subscribe(async value =>{
      const response:any = await this.authService.checkUsername(value).toPromise();
      this.usernameExists = response.exists
      console.log(response);
      
    })
  }

  async openModal(component) {
    const modal = await this.modalController.create({
      component: component,
    });
    await modal.present();
  }

  async register(form: NgForm) {
    if (form.valid && this.usernameExists == false) {
      const response: SuccessRegistry = await this.authService.registerUser(form.value).toPromise();
      if (response.success) {
        await this.tokenService.setToken(response.access_token);
        localStorage.setItem('name', response.payload.name);
        this.router.navigateByUrl('home');
      } else {
        alert('Servicio no disponible, por favor intentelo mas tarde')
      }
    }
  }

 
  checkValidUsername(username){
    this.usernameSource.next(username);
  }


}
