import { Component, OnInit } from '@angular/core';
import { AuthService } from "src/app/services/auth.service";
import { TokenService } from "src/app/services/token.service";
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  errorCredentials: boolean = false;
  loading: boolean = false;
  viewPass: boolean = false;;
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  async login(form: NgForm) {
    if (form.valid) {
      this.errorCredentials = false;
      this.loading = true;
      const username = form.value.username;
      const password = form.value.password;

      const response = await this.authService.signInWithUsernameAndPassword(username, password).toPromise();
      this.loading = false;
      if (response.success) {
        await this.tokenService.setToken(response.access_token);
        localStorage.setItem('name', response.payload.name);
        this.router.navigateByUrl('home');
      } else {
        this.errorCredentials = true;
        //form.controls.password.reset('');
      }
    }
  }

}
