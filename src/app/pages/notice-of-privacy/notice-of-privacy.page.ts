import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-notice-of-privacy',
  templateUrl: './notice-of-privacy.page.html',
  styleUrls: ['./notice-of-privacy.page.scss'],
})
export class NoticeOfPrivacyPage {

  constructor(private modalController: ModalController) { }
 
  dismiss(){
    this.modalController.dismiss();
  }

}
