import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';

import { from } from 'rxjs'
import { Injectable } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { TokenService } from "../services/token.service";
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    public router: Router,
    //public toastController: ToastController,
    public auth: AuthService,
    private tokenService: TokenService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return from(this.handle(request, next));
  }

  async handle(request: HttpRequest<any>, next: HttpHandler) {
    const { value } = await this.tokenService.getToken()
    if (value) {
      request = request.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + value
        }
      });
    }
    if (!request.headers.has('Content-Type') && !request.headers.has('no-change-content')) {
      request = request.clone({
        setHeaders: {
          'content-type': 'application/json'
        }
      });
    }
    request = request.clone({
      headers: request.headers.set('Accept', 'application/json')
    });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('event--->>>', event);
        }
        return event;
      }),
      // retry(2),
      catchError(this.handleError.bind(this)),
    ).toPromise();
  }

  async toastMessage(msg) {

  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.

    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      if (error.status == 401) { // unautorized
        //redirect to login
        debugger
        console.log('unautenticated');
        this.tokenService.deleteToken();
        this.router.navigateByUrl('/login');

        // this.router.navigateByUrl('login');
      } else {
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
        // alert('No se ha podido procesar la solicitud, por favor intente más tarde');
      }
    }
    //add more errors types if you need it

    return throwError(
      'Por favor intentelo mas tarde');
  }

}